/**
 * @file
 * Contains the functionality for refreshing blocks.
 */

(function ($, Drupal, drupalSettings, once) {

  var settings = drupalSettings.ajax_wrapper_settings;

  Drupal.behaviors.ajaxWrapper = {

    /**
     * Attach function.
     *
     * @param context
     *   The context.
     */
    attach: function (context) {

      if (settings.length <= 0) {
        return;
      }

      for (var i = 0; i <= settings.length; i++) {
        if (!settings[i]) {
          continue;
        }

        var settingsArray = settings[i];
        var wrapper = new AjaxWrapper(settingsArray, context);
        wrapper.attachAjaxWrapper();
      }
    },
  };

  class AjaxWrapper {

    is_refreshing = false;

    constructor(ajaxWrapperSettings, context) {
      this.ajaxWrapperSettings = ajaxWrapperSettings;
      this.wrapperId = ajaxWrapperSettings['wrapper_id'];
      this.$wrapper = $('#' + this.wrapperId);
      this.wrapperClasses = ajaxWrapperSettings['wrapper_classes'];
      this.context = context;
    }

    attachAjaxWrapper() {
      var self = this;
      self.attachLinkListeners();
      self.attachWindowPopStateListener();
    }

    attachLinkListeners() {
      for (var i = 0; i < this.wrapperClasses.length; i++) {
        var wrapperClass = this.wrapperClasses[i];
        this.attachLinkListenerForClass(wrapperClass);
        this.attachFormListenerForClass(wrapperClass);
      }
    }

    attachLinkListenerForClass(wrapperClass) {
      var self = this;
      var $links = this.$wrapper.find('.' + wrapperClass).find('a');

      $(once('init_link', $links, this.context)).on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        self.refreshWrapperByUrl(href);
        return false;
      });
    }

    attachFormListenerForClass(wrapperClass) {
      var self = this;
      var $forms = this.$wrapper.find('.' + wrapperClass).find('form');

      $(once('submit_form', $forms, this.context)).submit(function (e) {
        self.refreshBlockByFormSubmit($(this));
        e.preventDefault();
        return false;
      });
    }

    /**
     * Refreshes a block by form submit.
     */
    refreshBlockByFormSubmit($form) {
      this.refreshAjaxWrapper({
        'form_data': $form.serialize(),
        'url': drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + drupalSettings.path.currentPath,
        'ajax_wrapper_settings': this.ajaxWrapperSettings,
      });
    }

    refreshWrapperByUrl(url) {
      // If the page starts with arguments, prefix the current path.
      var pos = url.indexOf('?');

      if (pos === 0) {
        url = drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + drupalSettings.path.currentPath + url;
      }
      this.refreshAjaxWrapper({
        'url': url,
        'ajax_wrapper_settings': this.ajaxWrapperSettings,
      });
    }

    /**
     * Attaches windows by pop state.
     */
    attachWindowPopStateListener() {
      window.addEventListener('popstate', function (event) {
        if (event.state) {
          var selector = event.state.selector;
          var content = event.state.content;
          var wrapper = event.state.wrapper;
          $(selector).html(content);
          $('#' + wrapper).find('[data-once]').removeAttr('data-once');
        }
      }, {once: true});

      window.history.replaceState(
        {'selector': 'body', 'content': $('body').html(), 'wrapper': this.wrapperId},
        null,
        window.location.pathname + window.location.search
      );
    }

    refreshAjaxWrapper(data) {
      var self = this;
      this.$body = $("html");
      self.$body.addClass("block-refreshing");
      data.current_path = Drupal.url(drupalSettings.path.currentPath);

      if (!this.is_refreshing) {
        this.refreshing = true;

        var loadAjax = Drupal.ajax({
          url: self.ajaxWrapperSettings.ajax_url,
          method: 'POST',
          submit: data,
        });

        loadAjax.error = function(xmlhttp, uri) {
          self.$body.removeClass("block-refreshing");
          self.refreshing = false;
        }

        loadAjax.execute().done(function() {
          Drupal.attachBehaviors();
          self.$body.removeClass("block-refreshing");
          self.refreshing = false;
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings, once);



