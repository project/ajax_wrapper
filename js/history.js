(function ($, Drupal) {
  /**
   * Store content in history command.
   *
   * @param {Drupal.Ajax} [ajax]
   *   A {@link Drupal.ajax} object.
   * @param {object} response
   *   Ajax response.
   * @param {string} response.selector
   *   Selector to use.
   * @param {array} response.data
   *   Data to store.
   */
  Drupal.AjaxCommands.prototype.storeHistory = function (ajax, response) {
    if (!!window.history || !!window.history.pushState) {
      const content = $(response.selector).html();
      const path = response.path;
      window.history.pushState(
        { selector: response.selector, content },
        '',
        path,
      );
    }
  };

})(jQuery, Drupal);
