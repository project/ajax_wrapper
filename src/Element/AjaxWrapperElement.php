<?php

namespace Drupal\ajax_wrapper\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElementBase;
use Drupal\Core\Url;

/**
 * Provides a render element for adding Ajax to a render element.
 *
 * Holds an array whose values control the Ajax behavior of the element.
 *
 * @ingroup ajax
 *
 * @RenderElement("ajax_wrapper")
 */
class AjaxWrapperElement extends RenderElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#pre_render' => [
        static::class . '::preRenderAjaxWrapper',
      ],
      '#type' => 'ajax_wrapper',
      '#id' => 'ajax-wrapper',
      '#theme' => 'ajax_wrapper',
      '#ajax_wrapper_classes' => [
        'ajax-wrapper-classes',
      ],
      '#ajax_wrapper_attributes' => [],
      '#ajax_callback' => [],
      '#attached' => [
        'library' => 'ajax_wrapper/ajax_wrapper',
      ],
    ];
  }

  /**
   * Pre render the ajax wrapper functionality.
   *
   * @param array $element
   *   The ajax wrapper element.
   *
   * @return array
   *   Array containing the ajax wrapper.
   */
  public static function preRenderAjaxWrapper(array $element) {
    if (empty($element['#ajax_callback'])) {
      throw new \Exception('You must specify an ajax callback');
    }

    // First, just render the callback as we would normally.
    $callback = reset($element['#ajax_callback']);
    $callbackFunction = $callback[0] ?? NULL;
    $callbackArguments = $callback[1] ?? [];

    if (empty($element['#ajax_wrapper_content'])) {
      /** @var \Drupal\ajax_wrapper\Utility\AjaxWrapperCallbackUtility */
      $callbackUtility = \Drupal::service('ajax_wrapper.utility.callback');

      $result = $callbackUtility->doCallback(
        $callbackFunction, $callbackArguments, 'callback'
      );

      $element['#ajax_wrapper_content'] = $result;
    }

    if (empty($element['#ajax_url'])) {
      $element['#ajax_url'] = Url::fromRoute(
        'ajax_wrapper.refresh'
      )->toString();
    }

    // Attach the attributes.
    $element['#ajax_wrapper_attributes']['id'] = $element['#id'];

    // Attached the javascript Drupal settings.
    $element['#attached']['drupalSettings']['ajax_wrapper_settings'][] = [
      'wrapper_id' => $element['#id'] ?? Html::getUniqueId('ajax_wrapper'),
      'wrapper_classes' => $element['#ajax_wrapper_classes'] ?? [],
      'method' => $element['#ajax_method'] ?? 'html',
      'ajax_url' => $element['#ajax_url'],
      'callback' => [
        'function' => $callbackFunction,
        'arguments' => $callbackArguments,
      ],
    ];

    return $element;
  }

}
