<?php

namespace Drupal\ajax_wrapper\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Provides an AJAX command to store history of the search.
 */
class StoreHistoryCommand implements CommandInterface {

  /**
   * A CSS selector string.
   *
   * @var string
   */
  protected $selector;

  /**
   * The path..
   *
   * @var string
   */
  protected $path;

  /**
   * Constructs a \Drupal\voka_search\Ajax\StoreSearchHistoryCommand object.
   *
   * @param string $selector
   *   A CSS selector.
   * @param string $path
   *   The path..
   */
  public function __construct($selector, $path) {
    $this->selector = $selector;
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'storeHistory',
      'selector' => $this->selector,
      'path' => $this->path,
    ];
  }

}
