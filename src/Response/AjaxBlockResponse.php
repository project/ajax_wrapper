<?php

namespace Drupal\ajax_overview_block\Response;

use Drupal\Core\Ajax\AjaxResponse;

/**
 * This class extends the ajax block response.
 *
 * This way we can add additional data next to the commands.
 *
 * @package Drupal\ajax_overview_block\Response
 */
class AjaxBlockResponse extends AjaxResponse {

  /**
   * Add a key|value pair as response data.
   *
   * @param string $key
   *   The key of the data element.
   * @param mixed $value
   *   The value of the data element.
   */
  public function addDataKey(string $key, $value) {
    $this->commands[$key] = $value;
  }

}
